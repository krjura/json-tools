This project has two main branches *master* and *release*. 
Master branch is the development branch. All new feature are merged into this branch.
Release branch is the production branch. Once master branch is feature ready it should be merged here.

Feature development flow:
* create a new branch called feature-#\<issue number\> where \<issue number\> is the number of the issue in bitbucket issue tracker.
* do your work on the branch.
* Once tested merge the branch on master. Use fast forward where applicable.

release flow:
* bump the version on the master branch.
* merge features to the master branch.
* once all features are ready merge the master with the release branch. Prefer fast forward merge then possible.
* bump the version on the master.