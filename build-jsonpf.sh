#!/bin/bash

DIR=$(pwd)
BUILD_DIR=/opt/build
NAME=jsonpf

./gradlew -p json-pretty-formater build installDist
docker run \
  -v $DIR:$BUILD_DIR \
  docker.krjura.org/json-tools/graalvm-java-11:1 \
  bash -c "native-image --no-fallback -H:+ReportExceptionStackTraces -H:Name=$NAME --initialize-at-build-time --allow-incomplete-classpath --class-path 'json-pretty-formater/build/install/json-pretty-formater/lib/*' org.krjura.tools.json.jsonprettyformater.Formatter; chown $UID $NAME"

mv $NAME json-pretty-formater/
