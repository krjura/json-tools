This project contains various tools for dealing with JSON.

The following tools are currently available:

# json-pretty-formatter

Is used to pretty format json over the console. It can be used in combination with linux pipes

```
cat filename.json | jsonpf -
```

It can also be used with a --in-file argument

```
jsonpf --in-file filename.json
```

If you have a file or stream containing multiple json documents use --stream flag

```
jsonpf --stream --in-file filename.json
```

If you want formatted json to be printed to a file instead of terminal use --out-file

```
jsonpf --in-file in.json --out-file out.json
```

## Binaries

Binaries can be downloaded from [Official repository](https://repo.krjura.org/service/rest/repository/browse/public-artifacts/json-tools/). Number in the view represents a version. Binaries are currently only available for Linux.

## APT Repository

There is also an APT repository available. In order to use it add the following entry to /etc/apt/source.list or /etc/apt/source.list.d/krjura.org.list

```
deb [arch=amd64] https://repo.krjura.org/repository/debian-public/ stable main
```

After adding the entry install the apt key with:

```
wget -qO - https://repo.krjura.org/repository/public-artifacts/debian-public/gpg/public.key | sudo apt-key add -
```

add install the tool with 
```
sudo apt-get update
sudo apt-get install jsonpf
```

## YUM Repository

YUM/RPM repository is also available. In order to use it add the following entry to /etc/yum.repos.d/krjura.org

```
[krjuraorg]
name=krjura.org
baseurl=https://repo.krjura.org/repository/rpm-public/linux/x86_64
enabled=1
gpgcheck=0
```

add install the tool with

```
sudo yum install jsonpf
```
 

