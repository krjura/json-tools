#!/bin/bash

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
echo "Running in $SCRIPT_DIR"

cd $SCRIPT_DIR
native-image \
  --no-fallback \
  -H:+ReportExceptionStackTraces -H:Name=jsonpf \
  --initialize-at-build-time --allow-incomplete-classpath \
  --class-path 'build/install/json-pretty-formater/lib/*' \
  org.krjura.tools.json.jsonprettyformater.Formatter