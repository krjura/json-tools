#!/bin/bash

SCRIPT_DIR=$(dirname "$(readlink -f "$0")")
echo "Running in $SCRIPT_DIR"
cd $SCRIPT_DIR

CURRENT_VERSION=$(cat currentVersion)

curl --fail -u $KRJURA_NEXUS3_USERNAME:$KRJURA_NEXUS3_PASSWORD \
  -X PUT -H "Content-Type: application/octet-stream" \
  --data-binary "@jsonpf" \
  https://repo.krjura.org/repository/public-artifacts/json-tools/${CURRENT_VERSION}/jsonpf

curl --fail -u $KRJURA_NEXUS3_USERNAME:$KRJURA_NEXUS3_PASSWORD \
  -X POST -H "Content-Type: multipart/form-data" \
  --data-binary "@build/distributions/jsonpf_${CURRENT_VERSION}_amd64.deb" \
  https://repo.krjura.org/repository/debian-public/

curl --fail -u $KRJURA_NEXUS3_USERNAME:$KRJURA_NEXUS3_PASSWORD \
  -X PUT -H "Content-Type: multipart/form-data" \
  --data-binary "@build/distributions/jsonpf-${CURRENT_VERSION}-1.x86_64.rpm" \
  https://repo.krjura.org/repository/rpm-public/linux/x86_64/json-tools/jsonpf-${CURRENT_VERSION}-1.x86_64.rpm