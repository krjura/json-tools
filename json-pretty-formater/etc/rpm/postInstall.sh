#!/bin/bash

chown -R root:root /opt/json-tools/jsonpf
chmod -R o-w,o+rx,ug-w,ug+rx /opt/json-tools/jsonpf