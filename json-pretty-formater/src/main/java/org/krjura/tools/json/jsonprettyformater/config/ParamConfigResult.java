/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.config;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.krjura.tools.json.jsonprettyformater.utils.ProcessingResult;

public class ParamConfigResult {

  private ParamConfigResult() {
    // util
  }

  public static ProcessingResult<ParamConfig> success(ParamConfig data) {
    Objects.requireNonNull(data);

    return new ProcessingResult<>(true, data, Collections.emptyList());
  }

  public static ProcessingResult<ParamConfig> error(List<String> errors) {
    Objects.requireNonNull(errors);

    return new ProcessingResult<>(false, null, errors);
  }
}
