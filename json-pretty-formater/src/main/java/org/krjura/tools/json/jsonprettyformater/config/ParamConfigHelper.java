/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import org.krjura.tools.json.jsonprettyformater.params.Param;
import org.krjura.tools.json.jsonprettyformater.params.parsers.HelpParser;
import org.krjura.tools.json.jsonprettyformater.params.parsers.InFileParamParser;
import org.krjura.tools.json.jsonprettyformater.params.parsers.OutFileParamParser;
import org.krjura.tools.json.jsonprettyformater.params.parsers.ParamParser;
import org.krjura.tools.json.jsonprettyformater.params.parsers.ParserResult;
import org.krjura.tools.json.jsonprettyformater.params.parsers.StreamParser;
import org.krjura.tools.json.jsonprettyformater.params.parsers.SystemInParser;

public class ParamConfigHelper {

  private static final List<ParamParser> parsers = List.of(
      new HelpParser(),
      new SystemInParser(),
      new InFileParamParser(),
      new OutFileParamParser(),
      new StreamParser()
  );

  private ParamConfigHelper() {
    // util
  }

  public static Map<String, Param> parseArgs(String[] args) {
    Objects.requireNonNull(args);

    var params = new HashMap<String, Param>();

    var index = 0;
    var n = args.length;

    while (index < n) {
      final int currentIndex = index;

      var result = parseAtCurrentIndex(args, currentIndex);

      if (result.isPresent()) {
        var resultParam = result.get();
        addToParams(params, resultParam);
        index = resultParam.currentIndex();
      } else {
        addToParams(args, currentIndex, params);
        index = index + 1;
      }
    }

    return params;
  }

  private static void addToParams(String[] args, int currentIndex, HashMap<String, Param> params) {
    String currentParam = args[currentIndex];
    params.put(currentParam, Param.of(currentParam));
  }

  private static void addToParams(HashMap<String, Param> params, ParserResult resultParam) {
    params.put(resultParam.param().name(), resultParam.param());
  }

  private static Optional<ParserResult> parseAtCurrentIndex(String[] args, int currentIndex) {
    return parsers
        .stream()
        .map(paramParser -> paramParser.parse(currentIndex, args))
        .filter(ParserResult::isValid)
        .findFirst();
  }
}
