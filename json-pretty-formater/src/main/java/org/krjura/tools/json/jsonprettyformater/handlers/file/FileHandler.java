/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.handlers.file;

import static java.nio.file.Files.exists;
import static java.nio.file.Files.isDirectory;

import java.nio.file.Paths;
import java.util.Objects;
import org.krjura.tools.json.jsonprettyformater.config.ParamConfig;
import org.krjura.tools.json.jsonprettyformater.handlers.exit.ExitHandler;
import org.krjura.tools.json.jsonprettyformater.handlers.logger.Logger;

public class FileHandler {

  private final ExitHandler exitHandler;

  private final Logger logger;

  private FileHandler(
      final ExitHandler exitHandler,
      final Logger logger) {

    this.exitHandler = Objects.requireNonNull(exitHandler);
    this.logger = Objects.requireNonNull(logger);
  }

  public static FileHandler of(ExitHandler exitHandler, Logger logger) {
    return new FileHandler(exitHandler, logger);
  }

  public void checkFile(ParamConfig config) {
    Objects.requireNonNull(config);

    if (isDirectory(Paths.get(config.getInFile()))) {
      logger.println("File %s is a directory", config.getInFile());
      this.exitHandler.exit(1);
    }

    if (!exists(Paths.get(config.getInFile()))) {
      logger.println("File %s does not exists", config.getInFile());
      this.exitHandler.exit(1);
    }
  }
}
