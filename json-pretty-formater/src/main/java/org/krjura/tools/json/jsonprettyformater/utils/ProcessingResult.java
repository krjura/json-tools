/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.utils;

import java.util.List;
import java.util.function.Consumer;

public class ProcessingResult<T> {

  private final boolean success;

  private final T data;

  private List<String> errors;

  public ProcessingResult(boolean success, T data, List<String> errors) {
    this.success = success;
    this.data = data;
    this.errors = errors;
  }

  public ProcessingResult<T> onSuccess(Consumer<T> consumer) {
    if (this.success) {
      consumer.accept(this.data);
    }

    return this;
  }

  public ProcessingResult<T> onFailure(Consumer<List<String>> consumer) {
    if (!this.success) {
      consumer.accept(this.errors);
    }

    return this;
  }

  public boolean isSuccess() {
    return success;
  }

  public T getData() {
    return data;
  }

  public List<String> getErrors() {
    return errors;
  }
}
