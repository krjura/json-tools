/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.handlers.logger;

import static java.util.Objects.requireNonNull;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class FileLogger implements Logger {

  private static final byte[] NEW_LINE_BYTES = "\n".getBytes(StandardCharsets.UTF_8);

  private final String filename;
  private final BufferedOutputStream fos;
  private final Logger logger;

  public FileLogger(String filename, Logger logger) throws IOException {
    this.filename = requireNonNull(filename);
    this.fos = new BufferedOutputStream(new FileOutputStream(requireNonNull(filename)));
    this.logger = requireNonNull(logger);
  }

  @Override
  public Logger println(String message) {
    try {
      this.fos.write(message.getBytes(StandardCharsets.UTF_8));
      this.fos.write(NEW_LINE_BYTES);
    } catch (IOException e) {
      logger.println("cannot write output to file %s. Error was %s", filename, e.getMessage());
    }

    return this;
  }

  @Override
  public Logger println(String message, Object... args) {
    try {
      this.fos.write(Logger.format(message, args).getBytes(StandardCharsets.UTF_8));
      this.fos.write(NEW_LINE_BYTES);
    } catch (IOException e) {
      logger.println("cannot write output to file %s. Error was %s", filename, e.getMessage());
    }

    return this;
  }

  @Override
  public Logger print(String message) {
    try {
      this.fos.write(message.getBytes(StandardCharsets.UTF_8));
    } catch (IOException e) {
      logger.println("cannot write output to file %s. Error was %s", filename, e.getMessage());
    }

    return this;
  }

  @Override
  public Logger print(String message, Object... args) {
    try {
      this.fos.write(Logger.format(message, args).getBytes(StandardCharsets.UTF_8));
    } catch (IOException e) {
      logger.println("cannot write output to file %s. Error was %s", filename, e.getMessage());
    }

    return this;
  }

  @Override
  public void close() {
    try {
      this.fos.close();
    } catch (IOException e) {
      logger.println("cannot close writer. Error was %s", e.getMessage());
    }
  }
}
