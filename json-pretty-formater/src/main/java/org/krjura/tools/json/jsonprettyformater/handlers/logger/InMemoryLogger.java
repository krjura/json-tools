/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.handlers.logger;

import java.util.ArrayList;
import java.util.List;

public class InMemoryLogger implements Logger {

  private List<String> logs = new ArrayList<>();

  @Override
  public Logger println(String message) {
    return saveLogEntry(message);
  }

  @Override
  public Logger println(String message, Object... args) {
    return saveLogEntry(message, args);
  }

  @Override
  public Logger print(String message) {
    return saveLogEntry(message);
  }

  @Override
  public Logger print(String message, Object... args) {
    return saveLogEntry(message, args);
  }

  private Logger saveLogEntry(String message, Object[] args) {
    this.logs.add(Logger.format(message, args));

    return this;
  }

  private Logger saveLogEntry(String message) {
    this.logs.add(message);

    return this;
  }

  @Override
  public void close() {
    // nothing to close
  }

  public List<String> getLogs() {
    return logs;
  }
}
