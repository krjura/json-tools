/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.krjura.tools.json.jsonprettyformater.config.ParamConfig;
import org.krjura.tools.json.jsonprettyformater.config.ParamConfigParser;
import org.krjura.tools.json.jsonprettyformater.ex.FormatterException;
import org.krjura.tools.json.jsonprettyformater.handlers.exit.ExitHandler;
import org.krjura.tools.json.jsonprettyformater.handlers.exit.SystemExitHandler;
import org.krjura.tools.json.jsonprettyformater.handlers.file.FileHandler;
import org.krjura.tools.json.jsonprettyformater.handlers.logger.FileLogger;
import org.krjura.tools.json.jsonprettyformater.handlers.logger.Logger;
import org.krjura.tools.json.jsonprettyformater.handlers.logger.SystemOutLogger;

public class Formatter {

  private static final String CONST_OPEN_OBJECT = "{";
  private static final String CONST_OPEN_ARRAY = "[";
  private static final String CONST_CLOSED_ARRAY = "]";
  private static final String CONST_CLOSED_OBJECT = "}";
  private static final String CONST_DOUBLE_QUOTES = "\"";
  private static final String CONST_NULL = "null";
  private static final String CONST_COMMA = ",";
  private static final String CONST_NEW_LINE = "\n";
  private static final String CONST_LEVEL_1 = "  ";
  private static final String CONST_LEVEL_2 = "    ";
  private static final String CONST_LEVEL_3 = "      ";
  private static final String CONST_LEVEL_4 = "        ";
  private static final String CONST_LEVEL_5 = "          ";
  private static final String CONST_LEVEL_6 = "            ";
  private static final String CONST_LEVEL_7 = "              ";
  private static final String CONST_LEVEL_8 = "                ";
  private static final String CONST_LEVEL_9 = "                  ";
  private static final String CONST_LEVEL_10 = "                    ";

  private final FileHandler fileHandler;
  private final ExitHandler exitHandler;
  private final Logger logger;
  private Logger outputLogger;

  public Formatter(ExitHandler exitHandler, Logger logger) {
    this.exitHandler = Objects.requireNonNull(exitHandler);
    this.logger = Objects.requireNonNull(logger);

    this.fileHandler = FileHandler.of(exitHandler, logger);
  }

  public void format(String[] args) {
    Objects.requireNonNull(args);

    ParamConfigParser
        .parse(args)
        .onSuccess(this::processSuccessfulResult)
        .onFailure(this::processErrorResult);
  }

  public void close() {
    this.logger.close();
    this.outputLogger.close();
  }

  private void processErrorResult(List<String> errors) {
    errors.forEach(logger::println);
    exitHandler.exit(1);
  }

  private void processSuccessfulResult(ParamConfig config) {
    setupOutputLogger(config);

    if (config.printHelp()) {
      printHelp();
    } else {
      printFormattedJson(config, config.isStreamingAllowed());
    }
  }

  private void printFormattedJson(ParamConfig config, boolean streamingAllowed) {
    try (var reader = readJsonNode(config)) {
      writeNode(reader, streamingAllowed);
    } catch (IOException e) {
      this.logger.println("Cannot process json. Message was: %s", e.getMessage());
      this.exitHandler.exit(1);
    }
  }

  private void printHelp() {
    UsageGenerator.generateUsage(this.logger);
    this.exitHandler.exit(0);
  }

  private void writeNode(JsonReader reader, boolean streamingAllowed) {
    try {
      var state = new ParserState(streamingAllowed);

      while (state.active()) {

        state.currentToken(reader.peek());

        // we are waiting for the next element
        // allow only begin object
        if (reader.isLenient()) {
          writeNodeInLenientMode(reader, state);
        } else {
          writeNodeInNonLenientMode(reader, state);
        }

        state.currentToLastToken();
      }
    } catch (IOException e) {
      this.logger.println("Cannot process json. Message was: %s", e.getMessage());
      this.exitHandler.exit(1);
    }
  }

  private void writeNodeInLenientMode(JsonReader reader, ParserState state) throws IOException {

    var token = state.currentToken();

    if (token == JsonToken.BEGIN_OBJECT) {
      reader.beginObject();

      addNewLine(state);

      outputLogger.print(padLeft(state)).print(CONST_OPEN_OBJECT);
      state.incrementLevel();

      // do not have to be lenient any more
      reader.setLenient(false);
    } else if (token == JsonToken.END_DOCUMENT) {
      state.disable();
    } else {
      this.logger.println("Got invalid element. Expecting begin object but got %s", token);
      this.exitHandler.exit(1);
    }
  }

  private void writeNodeInNonLenientMode(JsonReader reader, ParserState state) throws IOException {

    var token = state.currentToken();

    switch (token) {
      case BEGIN_ARRAY:
        reader.beginArray();

        addComma(state);
        addNewLine(state);

        outputLogger.print(CONST_OPEN_ARRAY);
        state.incrementLevel();
        break;
      case END_ARRAY:
        reader.endArray();

        addNewLine(state);

        state.decrementLevel();
        outputLogger.print(padLeft(state)).print(CONST_CLOSED_ARRAY);
        break;
      case BEGIN_OBJECT:
        reader.beginObject();

        addComma(state);
        addNewLine(state);

        outputLogger.print(padLeft(state)).print(CONST_OPEN_OBJECT);
        state.incrementLevel();
        break;
      case END_OBJECT:
        reader.endObject();

        addNewLine(state);

        state.decrementLevel();
        outputLogger.print(padLeft(state)).print(CONST_CLOSED_OBJECT);

        // we have reached the end of this document
        // try with the other one
        if (state.streamingAllowed() && state.level() == 0) {
          reader.setLenient(true);
          // stop processing
        } else if (!state.streamingAllowed() && state.level() == 0) {
          state.active(false);
        }

        break;
      case NAME:
        String name = reader.nextName();

        addComma(state);
        addNewLine(state);

        outputLogger.print(padLeft(state)).print(CONST_DOUBLE_QUOTES).print(name).print("\": ");
        break;
      case STRING:

        addComma(state);
        addNewLine(state);
        addLevel(state);

        outputLogger.print(CONST_DOUBLE_QUOTES).print(reader.nextString()).print(CONST_DOUBLE_QUOTES);
        break;
      case NUMBER:

        addComma(state);
        addNewLine(state);
        addLevel(state);

        outputLogger.print(reader.nextString());
        break;
      case BOOLEAN:

        addComma(state);
        addNewLine(state);
        addLevel(state);

        outputLogger.print(Boolean.toString(reader.nextBoolean()));
        break;
      case NULL:
        reader.nextNull();

        addComma(state);
        addNewLine(state);
        addLevel(state);

        outputLogger.print(CONST_NULL);
        break;
      case END_DOCUMENT:
        state.disable();
        break;
      default:
        break;
    }
  }

  private void addLevel(ParserState state) {
    if (state.lastToken() != JsonToken.NAME) {
      outputLogger.print(padLeft(state));
    }
  }

  private void addComma(ParserState state) {
    var lastToken = state.lastToken();

    if (lastToken == JsonToken.END_ARRAY || lastToken == JsonToken.END_OBJECT) {
      outputLogger.print(CONST_COMMA);
    }

    if (lastToken == JsonToken.STRING
        || lastToken == JsonToken.NUMBER
        || lastToken == JsonToken.BOOLEAN
        || lastToken == JsonToken.NULL) {

      outputLogger.print(CONST_COMMA);
    }
  }

  private void addNewLine(ParserState state) {
    // there should be a new line after every
    switch (state.lastToken()) {
      case BEGIN_ARRAY:
      case END_ARRAY:
      case BEGIN_OBJECT:
      case END_OBJECT:
      case STRING:
      case NUMBER:
      case BOOLEAN:
      case NULL:
        outputLogger.print(CONST_NEW_LINE);
        break;
      default:
        break;
    }
  }

  private String padLeft(ParserState state) {
    switch (state.level()) {
      case 1:
        return CONST_LEVEL_1;
      case 2:
        return CONST_LEVEL_2;
      case 3:
        return CONST_LEVEL_3;
      case 4:
        return CONST_LEVEL_4;
      case 5:
        return CONST_LEVEL_5;
      case 6:
        return CONST_LEVEL_6;
      case 7:
        return CONST_LEVEL_7;
      case 8:
        return CONST_LEVEL_8;
      case 9:
        return CONST_LEVEL_9;
      case 10:
        return CONST_LEVEL_10;
      default:
        return IntStream
            .range(0, state.level())
            .mapToObj(i -> " ")
            .collect(Collectors.joining(""));
    }
  }

  private JsonReader readJsonNode(ParamConfig config) {
    if (config.hasInFile()) {
      return nodeFromFile(config);
    } else {
      return nodeFromSystemIn();
    }
  }

  private JsonReader nodeFromSystemIn() {
    return new JsonReader(new InputStreamReader(System.in));
  }

  private JsonReader nodeFromFile(ParamConfig config) {
    fileHandler.checkFile(config);

    try {
      return new JsonReader(new FileReader(config.getInFile()));
    } catch (IOException e) {
      this.logger.println("Cannot read file %s. Error was %s", config.getInFile(), e.getMessage());
      this.exitHandler.exit(1);
      throw new FormatterException("logger error", e);
    }
  }

  private void setupOutputLogger(ParamConfig config) {
    try {
      if (config.hasOutFile()) {
        this.outputLogger = new FileLogger(config.getOutFile(), logger);
      } else {
        this.outputLogger = this.logger;
      }
    } catch (IOException e) {
      logger.println("cannot setup output logger", e);
      System.exit(1);
    }
  }

  public static void main(String[] args) {
    var formatter = new Formatter(new SystemExitHandler(), new SystemOutLogger());
    formatter.format(args);
    formatter.close();
  }
}
