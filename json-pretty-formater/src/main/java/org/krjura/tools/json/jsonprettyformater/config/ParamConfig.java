/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.config;

public class ParamConfig {

  private final String inFile;
  private final String outFile;
  private final boolean readSystemIn;
  private final boolean printHelp;
  private final boolean streamingAllowed;

  private ParamConfig(
      String inFile, String outFile, boolean readSystemIn, boolean streamingAllowed, boolean printHelp) {

    this.inFile = inFile;
    this.outFile = outFile;
    this.readSystemIn = readSystemIn;
    this.printHelp = printHelp;
    this.streamingAllowed = streamingAllowed;
  }

  public static ParamConfig of(
      String inFile, String outFile, boolean fromSystemIn, boolean streamingAllowed, boolean printHelp) {

    return new ParamConfig(inFile, outFile, fromSystemIn, streamingAllowed, printHelp);
  }

  public boolean hasInFile() {
    return inFile != null;
  }

  public boolean hasOutFile() {
    return outFile != null;
  }

  public boolean readSystemIn() {
    return readSystemIn;
  }

  public String getInFile() {
    return inFile;
  }

  public String getOutFile() {
    return outFile;
  }

  public boolean printHelp() {
    return printHelp;
  }

  public boolean isStreamingAllowed() {
    return streamingAllowed;
  }
}
