/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.params.parsers;

import java.util.Objects;
import org.krjura.tools.json.jsonprettyformater.params.Param;

public class ParserResult {

  private final boolean valid;

  private final Param param;

  private final int currentIndex;

  private ParserResult(boolean valid, Param param, int currentIndex) {
    this.valid = valid;
    this.param = param;
    this.currentIndex = currentIndex;
  }

  public static ParserResult ofValid(Param param, int currentIndex) {
    Objects.requireNonNull(param);

    return new ParserResult(true, param, currentIndex);
  }

  public static ParserResult ofInvalid() {
    return new ParserResult(false, null, -1);
  }

  public boolean isValid() {
    return valid;
  }

  public Param param() {
    return param;
  }

  public int currentIndex() {
    return currentIndex;
  }
}
