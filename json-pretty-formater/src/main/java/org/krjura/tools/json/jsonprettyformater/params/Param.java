/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.params;

import java.util.List;
import java.util.Objects;

public class Param {

  private final String name;

  private final List<String> args;

  public Param(String name, List<String> args) {
    this.name = Objects.requireNonNull(name);
    this.args = Objects.requireNonNull(args);
  }

  public static Param of(String name, List<String> args) {
    return new Param(name, args);
  }

  public static Param of(String name, String arg) {
    return new Param(name, List.of(arg));
  }

  public static Param of(String name) {
    return new Param(name, List.of());
  }

  public String name() {
    return name;
  }

  public List<String> args() {
    return args;
  }

  public String firstArg() {
    if (args.isEmpty()) {
      return null;
    }

    return args.get(0);
  }
}
