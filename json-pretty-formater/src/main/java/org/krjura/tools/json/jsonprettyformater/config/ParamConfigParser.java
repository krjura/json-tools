/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.config;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.krjura.tools.json.jsonprettyformater.handlers.logger.Logger;
import org.krjura.tools.json.jsonprettyformater.params.Param;
import org.krjura.tools.json.jsonprettyformater.utils.ProcessingResult;

public class ParamConfigParser {

  private static final List<String> ALLOWED_COMMANDS = List.of(
      "-", "--in-file", "--out-file", "--help", "--stream");

  private ParamConfigParser() {
    // util
  }

  public static ProcessingResult<ParamConfig> parse(String[] args) {
    var params = ParamConfigHelper.parseArgs(args);

    String inFile = getFirstParam(params, "--in-file");
    String outFile = getFirstParam(params, "--out-file");
    boolean readSystemIn = params.containsKey("-");
    boolean printHelp = params.containsKey("--help");
    boolean allowStream = params.containsKey("--stream");

    var paramConfig = ParamConfig.of(inFile, outFile, readSystemIn, allowStream, printHelp);

    return verifyParams(params, paramConfig);
  }

  private static String getFirstParam(Map<String, Param> params, String key) {
    if (params.containsKey(key)) {
      return params.get(key).firstArg();
    }

    return null;
  }

  private static ProcessingResult<ParamConfig> verifyParams(Map<String, Param> params, ParamConfig paramConfig) {
    List<String> errors = verifyValidCommands(params);

    if (!errors.isEmpty()) {
      return ParamConfigResult.error(errors);
    }

    errors = verifyInputSource(paramConfig);

    if (!errors.isEmpty()) {
      return ParamConfigResult.error(errors);
    } else {
      return ParamConfigResult.success(paramConfig);
    }
  }

  private static List<String> verifyValidCommands(Map<String, Param> params) {
    List<String> notValidCommands = params
        .keySet()
        .stream()
        .filter(key -> !ALLOWED_COMMANDS.contains(key))
        .collect(Collectors.toList());

    return notValidCommands.isEmpty()
        ? List.of()
        : List.of(Logger.format("Unknown commands found: %s", notValidCommands));
  }

  private static List<String> verifyInputSource(ParamConfig config) {
    // do not check this if help is requested
    if (config.printHelp()) {
      return List.of();
    }

    if (config.hasInFile() && config.readSystemIn()) {
      return List.of("Must select single source of data. Please select file or system in");
    }

    if (!config.hasInFile() && !config.readSystemIn()) {
      return List.of("Must select single source of data. Please select file or system in");
    }

    return List.of();
  }
}
