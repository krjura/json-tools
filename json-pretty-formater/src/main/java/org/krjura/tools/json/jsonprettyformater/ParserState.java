/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater;

import com.google.gson.stream.JsonToken;

public class ParserState {

  private final boolean streamingAllowed;

  private boolean active = true;
  private JsonToken lastToken = JsonToken.END_DOCUMENT;
  private JsonToken currentToken;
  private int level = 0;

  public ParserState(boolean streamingAllowed) {
    this.streamingAllowed = streamingAllowed;
  }

  public boolean streamingAllowed() {
    return streamingAllowed;
  }

  public boolean active() {
    return active;
  }

  public void active(boolean active) {
    this.active = active;
  }

  public void disable() {
    active(false);
  }

  public JsonToken lastToken() {
    return lastToken;
  }

  public void lastToken(JsonToken lastToken) {
    this.lastToken = lastToken;
  }

  public JsonToken currentToken() {
    return currentToken;
  }

  public void currentToken(JsonToken currentToken) {
    this.currentToken = currentToken;
  }

  public void currentToLastToken() {
    this.lastToken = this.currentToken;
    this.currentToken = null;
  }

  public int level() {
    return level;
  }

  public void level(int level) {
    this.level = level;
  }

  public void decrementLevel() {
    this.level--;
  }

  public void incrementLevel() {
    this.level++;
  }
}
