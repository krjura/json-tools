/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.params.parsers;

import static java.util.Objects.checkIndex;
import static java.util.Objects.requireNonNull;

import java.util.ArrayList;
import org.krjura.tools.json.jsonprettyformater.params.Param;

class DefaultParamParser implements ParamParser {

  private final String key;
  private final int numberOfValues;

  DefaultParamParser(String key, int numberOfValues) {
    this.key = requireNonNull(key);
    this.numberOfValues = requireGreaterThenZero(numberOfValues);
  }

  private static int requireGreaterThenZero(int val) {
    if (val < 0) {
      throw new IllegalArgumentException("expecting larger then zero values but got " + val);
    }

    return val;
  }

  @Override
  public ParserResult parse(int index, String[] args) {
    var paramName = args[index];

    if (!paramName.equals(key)) {
      return ParserResult.ofInvalid();
    }

    if (numberOfValues == 0) {
      return ParserResult.ofValid(Param.of(paramName), index + 1);
    }

    checkIndex(index + numberOfValues, args.length);

    var values = new ArrayList<String>(numberOfValues);
    for (int i = 0; i < numberOfValues; i++) {
      int paramIndex = index + i + 1; // key + value index
      var paramValue = args[paramIndex];

      values.add(paramValue);
    }

    return ParserResult.ofValid(
      Param.of(key, values),
      index + numberOfValues + 1 // key + values
    );
  }
}
