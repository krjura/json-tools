/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater;

import org.krjura.tools.json.jsonprettyformater.handlers.logger.Logger;

public class UsageGenerator {

  private UsageGenerator() {
    // util
  }

  public static void generateUsage(Logger logger) {
    logger.println("Usage: jsonpf [options...]");
    logger.println("    -                       Use system in as source of data");
    logger.println("    --in-file <file path>   Use <file path> file as input data");
    logger.println("    --out-file <file path>  Use <file path> file as output data");
    logger.println("    --stream                Allows formatting multiple json documents in a single run");
    logger.println("    --help                  Print this help");
  }
}
