/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.performance;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class Generator {

  public static void main(String[] args) throws Exception {
    BufferedWriter fos = new BufferedWriter(new FileWriter("/tmp/demo.json"));

    fos.write("{\n" + "  \"names\": [");

    for (int i = 0; i < 5_000_000; i++) {
      fos.write("    {\n"
          + "      \"name\": \"kresimir\",\n"
          + "      \"surname\": \"jurasovic\",\n"
          + "      \"age\": \"49\",\n"
          + "      \"city\": \"Zagreb\",\n"
          + "      \"postal code\": \"10020\",\n"
          + "      \"country\": \"Republic of Croatia\",\n"
          + "      \"planet\": \"Planet of Earth\",\n"
          + "      \"galaxy\": \"The Milky Way\"\n"
          + "    },");
    }

    fos.write("{\"done\": true}");
    fos.write("]\n}");
    fos.flush();
    fos.close();
  }
}
