/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.config;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import org.junit.jupiter.api.Test;

public class ParamConfigParserTest {

  @Test
  public void parseValid() {
    String[] args = {"--in-file", "/tmp/file"};

    var result = ParamConfigParser.parse(args);
    assertThat(result.isSuccess()).isTrue();

    var config = result.getData();
    assertThat(config.hasInFile()).isTrue();
    assertThat(config.getInFile()).isEqualTo("/tmp/file");
  }

  @Test
  public void parseParamsWithoutFile() {
    String[] args = {"--directory", "/tmp/file"};

    var result = ParamConfigParser.parse(args);
    assertThat(result.isSuccess()).isFalse();

    List<String> errors = result.getErrors();
    assertThat(errors).hasSize(1);
    assertThat(errors.get(0)).isEqualTo("Unknown commands found: [--directory, /tmp/file]");
  }

  @Test
  public void failWhenFileAndSystemInPresent() {
    String[] args = {"--in-file", "/tmp/file", "-"};

    var result = ParamConfigParser.parse(args);
    assertThat(result.isSuccess()).isFalse();

    List<String> errors = result.getErrors();
    assertThat(errors).hasSize(1);
    assertThat(errors.get(0)).isEqualTo("Must select single source of data. Please select file or system in");
  }

  @Test
  public void failWhenFileAndSystemInAreNotPresent() {
    String[] args = {};

    var result = ParamConfigParser.parse(args);
    assertThat(result.isSuccess()).isFalse();

    List<String> errors = result.getErrors();
    assertThat(errors).hasSize(1);
    assertThat(errors.get(0)).isEqualTo("Must select single source of data. Please select file or system in");
  }

  @Test
  public void failOnInvalidParameter() {
    String[] args = {"--in-file", "/tmp/file", "--aaa", "demo", "bbb"};

    var result = ParamConfigParser.parse(args);
    assertThat(result.isSuccess()).isFalse();

    List<String> errors = result.getErrors();
    assertThat(errors).hasSize(1);
    assertThat(errors.get(0)).isEqualTo("Unknown commands found: [--aaa, bbb, demo]");
  }
}