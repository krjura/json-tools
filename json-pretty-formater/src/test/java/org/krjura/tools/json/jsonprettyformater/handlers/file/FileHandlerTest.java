/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.handlers.file;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.Files;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.krjura.tools.json.jsonprettyformater.config.ParamConfig;
import org.krjura.tools.json.jsonprettyformater.handlers.exit.NoOpExitHandler;
import org.krjura.tools.json.jsonprettyformater.handlers.logger.InMemoryLogger;

public class FileHandlerTest {

  private InMemoryLogger logger;
  private NoOpExitHandler exitHandler;

  @BeforeEach
  public void before() {
    this.logger = new InMemoryLogger();
    this.exitHandler = new NoOpExitHandler();
  }

  @Test
  public void failWhenDirectory() throws Exception {
    var handler = FileHandler.of(exitHandler, logger);

    var tmpDirectory = Files.createTempDirectory("json-pretty-formatter-");

    handler.checkFile(ParamConfig.of(tmpDirectory.toString(), null, false, false, false));

    assertThat(logger.getLogs()).hasSize(1);
    assertThat(logger.getLogs().get(0)).isEqualTo("File " + tmpDirectory.toString() + " is a directory");
  }

  @Test
  public void failWhenFileDoesNotExists() {
    var handler = FileHandler.of(exitHandler, logger);

    var filename = UUID.randomUUID().toString();

    handler.checkFile(ParamConfig.of(filename, null, false, false, false));

    assertThat(logger.getLogs()).hasSize(1);
    assertThat(logger.getLogs().get(0)).isEqualTo("File " + filename + " does not exists");
  }

  @Test
  public void fileExists() throws Exception {
    var handler = FileHandler.of(exitHandler, logger);

    var path = Files.createTempFile("json-pretty-formater-", ".tmp");

    handler.checkFile(ParamConfig.of(path.toString(), null, false, false, false));

    assertThat(logger.getLogs()).hasSize(0);
  }

}