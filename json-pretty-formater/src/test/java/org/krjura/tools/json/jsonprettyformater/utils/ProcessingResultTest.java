/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.utils;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.function.Consumer;
import org.junit.jupiter.api.Test;

public class ProcessingResultTest {

  @Test
  public void testData() {
    var result = new ProcessingResult<String>(true, "demo", List.of("error"));

    assertThat(result.isSuccess()).isTrue();
    assertThat(result.getData()).isEqualTo("demo");
    assertThat(result.getErrors()).hasSize(1);
    assertThat(result.getErrors().get(0)).isEqualTo("error");
  }

  @Test
  public void testFailureCallBack() {
    var result = new ProcessingResult<String>(false, "demo", List.of("error"));

    var callback = new CallbackTester<List<String>>();
    result.onFailure(callback);

    assertThat(callback.getData()).hasSize(1);
    assertThat(callback.getData().get(0)).isEqualTo("error");
  }

  @Test
  public void testSuccessCallBack() {
    var result = new ProcessingResult<String>(true, "demo", List.of("error"));

    var callback = new CallbackTester<String>();
    result.onSuccess(callback);

    assertThat(callback.getData()).isEqualTo("demo");
  }

  private static class CallbackTester<T> implements Consumer<T> {

    private T data;

    @Override
    public void accept(T data) {
      this.data = data;
    }

    public T getData() {
      return data;
    }
  }
}