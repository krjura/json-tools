/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater.config;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class ParamConfigHelperTest {

  @Test
  public void singleParameter() {
    String[] args = {"-f", "file"};

    var data = ParamConfigHelper.parseArgs(args);

    assertThat(data).hasSize(2);
    assertThat(data.get("-f")).isNotNull();
    assertThat(data.get("-f").name()).isEqualTo("-f");
    assertThat(data.get("-f").args()).hasSize(0);
    assertThat(data.get("file")).isNotNull();
    assertThat(data.get("file").name()).isEqualTo("file");
    assertThat(data.get("file").args()).hasSize(0);
  }

  @Test
  public void parameterWithEquals() {
    String[] args = {"--in-file", "value"};

    var data = ParamConfigHelper.parseArgs(args);

    assertThat(data).hasSize(1);
    assertThat(data.get("--in-file")).isNotNull();
    assertThat(data.get("--in-file").name()).isEqualTo("--in-file");
    assertThat(data.get("--in-file").args()).hasSize(1);
    assertThat(data.get("--in-file").firstArg()).isEqualTo("value");
  }

}