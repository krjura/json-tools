/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.krjura.tools.json.jsonprettyformater;

import static org.assertj.core.api.Assertions.assertThat;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.krjura.tools.json.jsonprettyformater.handlers.exit.NoOpExitHandler;
import org.krjura.tools.json.jsonprettyformater.handlers.logger.InMemoryLogger;

public class FormatterTest {

  @Test
  public void printHelp() {
    var logger = new InMemoryLogger();
    var exitHandler = new NoOpExitHandler();

    String[] args = {"--help"};

    var formatter = new Formatter(exitHandler, logger);
    formatter.format(args);

    assertThat(logger.getLogs())
        .hasSize(6);
    assertThat(logger.getLogs().get(0))
        .isEqualTo("Usage: jsonpf [options...]");
    assertThat(logger.getLogs().get(1))
        .isEqualTo("    -                       Use system in as source of data");
    assertThat(logger.getLogs().get(2))
        .isEqualTo("    --in-file <file path>   Use <file path> file as input data");
    assertThat(logger.getLogs().get(3))
        .isEqualTo("    --out-file <file path>  Use <file path> file as output data");
    assertThat(logger.getLogs().get(4))
        .isEqualTo("    --stream                Allows formatting multiple json documents in a single run");
    assertThat(logger.getLogs().get(5))
        .isEqualTo("    --help                  Print this help");
  }

  @Test
  public void formatFile() throws Exception {
    var logger = new InMemoryLogger();
    var exitHandler = new NoOpExitHandler();

    String[] args = {"--in-file", "test-examples/example.json"};

    var formatter = new Formatter(exitHandler, logger);
    formatter.format(args);

    String log = String.join("", logger.getLogs());
    assertThat(log).isEqualTo(loadExample());
  }

  @Test
  public void formatFileInToFileOutInStreamingMode() throws Exception {
    var logger = new InMemoryLogger();
    var exitHandler = new NoOpExitHandler();

    String filename = "/tmp/" + UUID.randomUUID().toString();

    String[] args = {
        "--in-file", "test-examples/streaming-example.json",
        "--out-file", filename,
        "--stream"
    };

    var formatter = new Formatter(exitHandler, logger);
    formatter.format(args);
    formatter.close();

    String log = String.join("", logger.getLogs());
    assertThat(log).isEqualTo("");

    var outputBytes = Files.readAllBytes(Path.of(filename));
    assertThat(new String(outputBytes, StandardCharsets.UTF_8)).isEqualTo(loadStreamingExample());
  }

  @Test
  public void formatFileInToFileOut() throws Exception {
    var logger = new InMemoryLogger();
    var exitHandler = new NoOpExitHandler();

    String filename = "/tmp/" + UUID.randomUUID().toString();

    String[] args = {
        "--in-file", "test-examples/example.json",
        "--out-file", filename
    };

    var formatter = new Formatter(exitHandler, logger);
    formatter.format(args);
    formatter.close();

    String log = String.join("", logger.getLogs());
    assertThat(log).isEqualTo("");

    var outputBytes = Files.readAllBytes(Path.of(filename));
    assertThat(new String(outputBytes, StandardCharsets.UTF_8)).isEqualTo(loadExample());
  }

  @Test
  public void failToFormatFileWhenJsonStream() throws Exception {
    var logger = new InMemoryLogger();
    var exitHandler = new NoOpExitHandler();

    String[] args = {"--in-file", "test-examples/streaming-example.json"};

    var formatter = new Formatter(exitHandler, logger);
    formatter.format(args);

    String log = String.join("", logger.getLogs());
    assertThat(log).isEqualTo(loadExample());
  }

  @Test
  public void formatFileInStreamingMode() throws Exception {
    var logger = new InMemoryLogger();
    var exitHandler = new NoOpExitHandler();

    String[] args = {"--in-file", "test-examples/streaming-example.json", "--stream"};

    var formatter = new Formatter(exitHandler, logger);
    formatter.format(args);

    String log = String.join("", logger.getLogs());
    assertThat(log).isEqualTo(loadStreamingExample());
  }

  private String loadExample() throws Exception {
    return Files.readString(Paths.get("test-examples/example-formatted.json"));
  }

  private String loadStreamingExample() throws Exception {
    return Files.readString(Paths.get("test-examples/streaming-example-formatted.json"));
  }
}